# My working setup

So, this is it. My system setup. This is purely for me to keep track of all change I make on this laptop, cause it's a tiny underpowered X60s I got for 40 quid but still works extremely well. There's nothing too special about it, except that it uses a cool menu in the bottom left, working tab thingies and wicd-compatible wifi applets in the polybar. It's entirely oriented around my own customisation of Debian, which uses runit + busybox/musl, wicd, alsa & Avahi mDNS. It's like Devuan, but not autistic, and I can't install quite a few desktop packages, but oh well, at least there's no Poettering inside. 
:^)

It's also got a few fonts and stuff, and works around the Win/Cmd key, rather than meta since I actually use Emacs on this thing.

### Dependancies

Requires you to install;

- Linux Desktop Files 

- rofi
- dmenu
- obconf
- polybar
- tint2
- compton

Optional Dependencies;

- Obmenu-generator
- Wayfire/Weston
- gmrun



### Keybinds

| Keys                     | Function                                     |
| ------------------------ | -------------------------------------------- |
| Mod4 + 1                 | Workspace 1                                  |
| Mod4 + shift + 2         | Move to workspace 2                          |
| Mod4 + t                 | Toggle titlebar                              |
| Mod4 + z                 | Iconfify (minimize)                          |
| Mod4 + u                 | Hide the working window (keeps titlebar)     |
| Mod4 + s                 | Keeps the selected window on every workspace |
| Mod4 + r                 | Resize the window                            |
| Mod4 + c                 | Close the window                             |
| Meta + tab               | Cycle between windows                        |
| Meta + shift + tab       | Cycle to previous window                     |
| Mod4 + Ctrl + right      | Move window to the right (grid placement)    |
| Mod4 + Ctrl + left       | Move window left (grid p.)                   |
| Mod4 + Ctrl + up         | Move window up (grid p.)                     |
| Mod4 + Ctrl + down       | Move window down (grid p.)                   |
| Mod4 + RET               | Launch a terminal emulator                   |
| Mod4 + d                 | Show rofi application launcher               |
| Mod4 + shift + g         | Launch Sublime Text                          |
| Mod4 + f                 | Open the file manager                        |
| Mod4 + r                 | Rofi -show drun                              |
| Print                    | Screenshot                                   |
| Meta + Print             | Screenshot a selection                       |
| Ctrl + Print             | Screenshot with a 3 second delay             |
| Mod4 + shift + r         | Reload Openbox                               |
| Mod4 + shift + Backspace | Restart the session                          |
| Mod4 + shift + q         | Log out the session                          |

### Screenshot

![Final Result](screenshot.png)

#### Wallpaper

![wall](wall.png)